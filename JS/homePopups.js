$(document).ready(function(){
        
    $('#sendCoins').click(function () {
        $.confirm({
            title: 'Send Coins',
            backgroundDismiss: 'Send',
            content: '<img src="images/search.png"><div class="checkbox popup-checkbox"><label><input type="checkbox" id="enableCheckbox">Thisari Patabendi</label></div><div class="checkbox popup-checkbox"><label><input type="checkbox" id="enableCheckbox">Sohani Wewelwala</label></div><div class="checkbox popup-checkbox"><label><input type="checkbox" id="enableCheckbox">Dilshan Perera</label></div><img class="img-responsive center-block" src="images/sendCoinsPopup.png">',
            buttons: {
                Send: function () {
//                      var $checkbox = this.$content.find('#enableCheckbox');
//                      return $checkbox.prop('checked');
                      },
                Cancel: function (){}
            }
        });                      
    });
                  
    $('#leaderboard').click(function () {
        $.confirm({
            title: 'Leaderboard',
            backgroundDismiss: 'OK',
            content: '<img src="images/LeaderboardPopup.png">',
            buttons: {
                OK: function () {
//                        var $checkbox = this.$content.find('#enableCheckbox');
//                        return $checkbox.prop('checked');
                    },
                    Cancel: function (){}
            }
        });                      
    });
                  
    $('#howToPlay').click(function () {
        $.confirm({
              title: 'How to Play',
              backgroundDismiss: 'Send',
              content: '<div class="listview-background"><ul class="no_bullet"><li><img class="popup_bullet" src="images/CoffeeCup.png">Go to one of our outlets</li><li><img class="popup_bullet" src="images/CoffeeCup.png">Recieve the notification for a clue</li><li><img class="popup_bullet" src="images/CoffeeCup.png">Scan the QR code</li><li><img class="popup_bullet" src="images/CoffeeCup.png">Play the game</li><li><img class="popup_bullet" src="images/CoffeeCup.png">Earn points and redeem</li></ul></div>',
              buttons: {
                  OK: function () {
//                          var $checkbox = this.$content.find('#enableCheckbox');
//                          return $checkbox.prop('checked');
                      },
                  Cancel: function (){}
              }
        });                      
    });
    
    $('#rateUs').click(function () {
        $.confirm({
            title: ' ',
            backgroundDismiss: 'Send',
            content: '<p class="popup_rate text-center">Your payment is successfull!! <br> Please rate us!</p><img class="img-responsive center-block starRating" src="images/star_rating.png">',
            buttons: {
                OK: function (){},
                Cancel: function (){}
            }
        });                      
    });
    
});